﻿/*Pocket.cs
 * Part of Virtual Pockets, a system for carrying things in VR
 * Pockets submodule
 * ???--->asimonso@mit.edu
 * February 2021
 * 
 * THIS SCRIPT GOES ON THE NEW BRUSH AREA OF THE BRUSH GENERATOR, 
 * NOT ON THE BRUSH GENERATOR ITSELF
 * 
 * Generates new brushes of the correct size
 * 
 * Currently should generate infinite brushes, not of a customizable size
 * The new ones also shouldn't work yet
 */
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushGenerator : MonoBehaviour
{
    public GameObject brushPrefab;
    private GameObject currentBrush;
    private GameObject previousBrush;
    private float brushSize;
    private bool okayToGenerate=true;//don't make a huge number of duplicate brushes
    public PaintSelector paintSelector;//so that we can tell brushes about it

    public void SetBrushSize(float newSize)
    {
        brushSize = newSize;
        //scale currentBrush
        currentBrush.transform.localScale = new Vector3(brushSize, brushSize, brushSize);
    }

    private void Start()
    {
        currentBrush = Instantiate(brushPrefab, transform.position, transform.rotation);
        currentBrush.transform.parent = gameObject.transform.parent;
        currentBrush.GetComponentInChildren<PaintbrushEnd>().paintSelector = paintSelector;
        okayToGenerate = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == currentBrush)//if we took the brush out of the new brush area
        {
            okayToGenerate = true;
            GenerateBrush();
        }
    }

    public void GenerateBrush()//Called by brush generator button
    {
        if(okayToGenerate)//if we took the brush out of the new brush area
        {
            currentBrush.transform.SetParent(null);
            previousBrush = currentBrush;
            currentBrush = Instantiate(brushPrefab, transform.position, transform.rotation);
            currentBrush.transform.parent = gameObject.transform.parent;
            currentBrush.GetComponentInChildren<PaintbrushEnd>().paintSelector = paintSelector;
            okayToGenerate = false;
        }
    }

}
