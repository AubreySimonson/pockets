﻿/*Pockets
 * Part of Virtual Pockets, a system for carrying things in VR
 * ???--->asimonso@mit.edu
 * January 2020
 * 
 * Creates a tool belt of many pockets.  
 * 
 * Attach to Player gameobject to use.  
 * 
 * Remember to link HeadTransform.
 * 
 * Stick a controller in your pocket to track hips
 * 
 */
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toolbelt : MonoBehaviour
{
    public Transform beltTransform;
    public GameObject toolbelt;
    private GameObject pocket1, pocket2, pocket3;//this is still all a little manual for your taste
    public HardwareLink hardwareLink;//manually attach


    // Start is called before the first frame update
    void Start()
    {
        toolbelt = new GameObject("Toolbelt");
        toolbelt.transform.parent = beltTransform;//right now you're using the left hand controller because you're being HACK
        //toolbelt.transform.position = beltTransform.position;
        toolbelt.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);


        //wow how not-modular of you
        //position hard-coding can be changed once the belt is being tracked
        pocket1 = new GameObject("Pocket1");
        pocket1.AddComponent<Pocket>();
        hardwareLink.pocket1 = pocket1.GetComponent<Pocket>();
        pocket1.transform.parent = toolbelt.transform;
        pocket1.transform.localPosition = new Vector3(-0.1f, 0.0f, 0.0f);

        pocket2 = new GameObject("Pocket2");
        pocket2.AddComponent<Pocket>();
        hardwareLink.pocket2 = pocket2.GetComponent<Pocket>();
        pocket2.transform.parent = toolbelt.transform;
        pocket2.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

        pocket3 = new GameObject("Pocket3");
        pocket3.AddComponent<Pocket>();
        hardwareLink.pocket3 = pocket3.GetComponent<Pocket>();
        pocket3.transform.parent = toolbelt.transform;
        pocket3.transform.localPosition = new Vector3(0.1f, 0.0f, 0.0f);
    }
}
