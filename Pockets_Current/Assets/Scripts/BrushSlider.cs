﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class BrushSlider : MonoBehaviour
{
    public SteamVR_ActionSet actionSetEnable;
    public SteamVR_Action_Boolean paint;
    //public GameObject paintbrush; //whatever you want to use to paint
    public Camera VRCamera;//this needs to be manually linked in the inspector
    public GameObject marker;
    private float brushScaleFactor;
    public BrushGenerator brushGenerator;
    public PaintSelector paintSelector;//so that we can know about the current brush

    // Start is called before the first frame update
    void Start()
    {
        brushScaleFactor = 3.0f;
        //paintbrush.transform.localScale = new Vector3(brushScaleFactor, brushScaleFactor, brushScaleFactor);
    }

    // Update is called once per frame
    void Update()
    {
        if (paint.GetState(SteamVR_Input_Sources.Any))//vr version of getmousebutton
        {
            RaycastHit hit;
            //you are having a really hard time with layer masks
            //you didn't figure out how to make it only hit customslider,
            //but you did figure out how to make it hit only non-default layers and  honestly close enough
            int layer_mask = LayerMask.GetMask("Default");
            if (Physics.Raycast(paintSelector.currentBrush.transform.position, paintSelector.currentBrush.transform.forward, out hit, layer_mask))
            {
                Debug.Log("Did Hit");
                if(hit.collider.gameObject.name == "SliderHitArea")
                {
                    //you need to convert everything to local coordinates so that it has the right axis and everything
                    Vector3 localHit = transform.InverseTransformPoint(hit.point);
                    float markerX = marker.transform.localPosition.x;
                    float markerY = marker.transform.localPosition.y;
                    float markerZ = marker.transform.localPosition.z;
                    //marker.transform.position = hit.point;
                    marker.transform.localPosition = new Vector3(markerX, localHit.y, markerZ);
                    brushScaleFactor = (0.5f + localHit.y) *5.0f;//this mapping is arbitrary and we can do whatever we want with it
                    brushGenerator.SetBrushSize(brushScaleFactor);
                    //paintbrush.transform.localScale = new Vector3(brushScaleFactor, brushScaleFactor, brushScaleFactor);
                }
            }
            //check if it intersects with this, output where
        }
    }
}
