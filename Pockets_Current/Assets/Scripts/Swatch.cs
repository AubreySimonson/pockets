﻿/*Pockets
 * Swatch
 * 
 * Painting Submodule
 * 
 * Works with PaintSelector script to create a menu of paints in a paint palette, which can be selected using a paintbrush 
 * 
 * asimonso@mit.edu--march2020
 * **/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swatch : MonoBehaviour
{
    public GameObject paintbrushEnd;
    public Color paint;
    private PaintSelector paintSelector;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Paint")
        {
            //paintSelector.SetSelectedPaint(this);
            Highlight();
        }
    }

    public void Highlight()
    {
        paintbrushEnd.GetComponent<Renderer>().material.color = paint;
    }

    public void SetPaint(Color newPaint)
    {
        paint = newPaint;
    }

    public void SetPaintSelector(PaintSelector newPaintSelector)
    {
        paintSelector = newPaintSelector;
    }
}
