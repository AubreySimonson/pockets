﻿/*Pocket.cs
 * Part of Virtual Pockets, a system for carrying things in VR
 * Pockets submodule
 * ???--->asimonso@mit.edu
 * February 2021
 * 
 * Goes on brush generator button
 */
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateBrushButton : MonoBehaviour
{
    public GameObject leftHand, rightHand;//doesn't matter if they're backwards, will work with one hand
    public BrushGenerator brushGenerator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == leftHand || other.gameObject == rightHand)
        {
            brushGenerator.GenerateBrush();
        }
    }
}
