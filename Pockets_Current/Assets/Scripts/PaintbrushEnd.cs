﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintbrushEnd : MonoBehaviour
{
    public PaintSelector paintSelector;
    public bool isCurrentBrush = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Paint")
        {
            Color newPaint = other.gameObject.GetComponent<Renderer>().material.color;
            gameObject.GetComponent<Renderer>().material.color = newPaint;
            if (isCurrentBrush)
            {
                paintSelector.SetSelectedPaint(newPaint);
            }
        }
    }
}
