﻿/*HardwareLink.cs
 * Part of Virtual Pockets, a system for carrying things in VR
 * Pockets submodule
 * ???--->asimonso@mit.edu
 * January 2020
 * 
 * This script handles all communication with the physical toolbelt itself.
 * 
 * Remember that things with api compatability are super weird.  This might help:
 * https://answers.unity.com/questions/1604233/systemioports-missing-for-unity-with-net-4x.html
 * 
 * If it isn't working, check that you're connected to the right port
 */
using System.IO.Ports;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class HardwareLink : MonoBehaviour
{
    SerialPort stream = new SerialPort("COM8", 115200, Parity.None, 8, StopBits.One); // Check bottom right corner of Arduino IDE for COM #
    private bool ispressed1, ispressed2, ispressed3, ispressed4, ispressed5, ispressed6;
    public Pocket pocket1, pocket2, pocket3, pocket4, pocket5, pocket6;
    private void Start()
    {
        stream.Open();
        stream.ReadTimeout = 10;//originally 50
        ispressed1 = false;
        ispressed2 = false;
        ispressed3 = false;
        ispressed4 = false;
        ispressed5 = false;
        ispressed6 = false;
        //for now, we're plugging the pockets in.  We don't want to keep doing that long-term.
    }// end start

    // Update is called once per frame
    void Update()
    {
        string deviceMsg = stream.ReadExisting();//flush
        deviceMsg = stream.ReadLine();
        deviceMsg = deviceMsg.Trim();
        //Debug.Log(deviceMsg);
        if (deviceMsg == "0")
        {
            ispressed1 = false;
            ispressed2 = false;
            ispressed3 = false;
            ispressed4 = false;
            ispressed5 = false;
            ispressed6 = false;
        }
        //not the most efficient or elegant code you've ever written but you'll take it
        else if(deviceMsg == "1")
        {
            if (ispressed1 == false)
            {
                ispressed1 = true;
                Debug.Log("You clicked button 1!");
                pocket1.Press();
            }
        }
        else if (deviceMsg == "2")
        {
            if (ispressed2 == false)
            {
                ispressed2 = true;
                Debug.Log("You clicked button 2!");
                pocket2.Press();
            }
        }
        else if (deviceMsg == "3")
        {
            if (ispressed3 == false)
            {
                ispressed3 = true;
                Debug.Log("You clicked button 3!");
                pocket3.Press();
            }
        }
        else if (deviceMsg == "4")
        {
            if (ispressed4 == false)
            {
                ispressed4 = true;
                Debug.Log("You clicked button 4!");
                pocket4.Press();
            }
        }
        else if (deviceMsg == "5")
        {
            if (ispressed5 == false)
            {
                ispressed5 = true;
                Debug.Log("You clicked button 5!");
                pocket5.Press();
            }
        }
        else if (deviceMsg == "6")
        {
            if (ispressed6 == false)
            {
                ispressed6 = true;
                Debug.Log("You clicked button 6!");
                pocket6.Press();
            }
        }
    }// end update


    void OnApplicationQuit()
    {

        stream.Close();

    }
}