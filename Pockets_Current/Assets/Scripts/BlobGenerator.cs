﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobGenerator : MonoBehaviour
{
    //goes on the "Generate Blob" button of the Pocket Color Selector.
    //Generates a blob which is the same color as the Color Descriptor next to it
    //when intersected by the hand.  
    //
    //REMEMBER TO PLUG IN THE HAND VARIABLES!


    public GameObject leftHand, rightHand;//doesn't matter if they're backwards, will work with one hand
    public GameObject colorDescriptor;//we should write a find call for this but haven't because lazy
    public GameObject blobPrefab;
    public GameObject instantiateHere;
    public PaintSelector paintSelector;//for telling blobs about it
    public GameObject paintbrushEnd;//for telling blobs about it

    // Start is called before the first frame update
    void Start()
    {
        //error handling!
        if(colorDescriptor == null)
        {
            Debug.LogWarningFormat("Hey!  You forgot to tell Blob Generator about the color descriptor on the same game object");
        }
        if(leftHand == null || rightHand == null)
        {
            Debug.LogWarningFormat("Hey!  Blob Generator doesn't know about both of your hands");
        }
        if (blobPrefab == null)
        {
            Debug.LogWarningFormat("Hey!  You forgot to tell blob generator about the blob prefab!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == leftHand || other.gameObject == rightHand)
        {
            GameObject newBlob = Instantiate(blobPrefab);
            newBlob.GetComponent<Renderer>().material.color = colorDescriptor.GetComponent<Renderer>().material.color;
            newBlob.transform.position = instantiateHere.transform.position;
            //newBlob.AddComponent<Swatch>();
            //newBlob.GetComponent<Swatch>().SetPaintSelector(paintSelector);
            //newBlob.GetComponent<Swatch>().paintbrushEnd = paintbrushEnd;
            //newBlob.GetComponent<Swatch>().SetPaint(newBlob.GetComponent<Renderer>().material.color);
        }
    }
}
