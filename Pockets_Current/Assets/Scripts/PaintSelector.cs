﻿/*Pockets
 * Paint Selector
 * 
 * Painting Submodule
 * 
 * Dynamically creates a menu of 2D sprites, attached to the palette gameobject.
 * Attach this script to an empty gameobject, and make that gameobject a child of whatever hand you want to have hold the palette (which can be nothing!).
 * Do not make the palette prefab(with the model) a child of the hand.  This script does that for you.
 * 
 * Another script called "Swatch" is its only dependency.
 * 
 * Remember to connect the palette prefab, paintbrush prefab, and paintbrush hand in the inspector.
 * Remember to tag the paintbrush as "Paint"
 * 
 * asimonso@mit.edu--march 2020
 * **/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintSelector : MonoBehaviour
{
    public Color selectedPaint;
    public GameObject currentBrush;//whatever brush we last picked up with our dominant hand
    public TexturePainter[] texturePainters;
    //public TexturePainter texturePainter;//we need to tell it about whatever paintbrush we're holding
    public SpriteColor colorPicker;//we need to tell it about whatever paintbrush we're holding
    public Valve.VR.InteractionSystem.Hand rightHand;


    public void SetSelectedPaint(Color newColor)
    {
        selectedPaint = newColor;
    }
    private void Update()
    {
        //check if we picked up a new paintbrush
        if (rightHand.currentAttachedObject != currentBrush)
        {
            if (rightHand.currentAttachedObject.tag == "brush")
            {
                //tell old brush that it is no longer being held
                if (currentBrush != null)//don't fail on first pickup
                {
                    currentBrush.GetComponentInChildren<PaintbrushEnd>().isCurrentBrush = false;
                }
                currentBrush = rightHand.currentAttachedObject;
                //tell new brush that it is the new brush
                currentBrush.GetComponentInChildren<PaintbrushEnd>().isCurrentBrush = true;
                //make sure we're painting with the right color
                //this is maybe not the most efficient way to say this
                selectedPaint = currentBrush.GetComponentInChildren<PaintbrushEnd>().gameObject.GetComponent<Renderer>().material.color;
                foreach(TexturePainter painter in texturePainters)
                {
                    painter.UpdateBrush(currentBrush);
                }
                colorPicker.UpdateBrush(currentBrush);
            }
        }
    }
}
