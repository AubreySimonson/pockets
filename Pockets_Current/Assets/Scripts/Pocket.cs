﻿/*Pocket.cs
 * Part of Virtual Pockets, a system for carrying things in VR
 * Pockets submodule
 * ???--->asimonso@mit.edu
 * January 2021
 * 
 * Creates an individual virtual pocket.  
 * 
 * --you just cut a bunch of things and that might cause problems for you
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocket : MonoBehaviour
{
    private GameObject pocketObj;//what is in the pocket?
    private GameObject heldObj;//what are you probably trying to put in the pocket?
    private GameObject respawnPoint;//if things reinstantiate in the pocket you can't ever get them out
    public Valve.VR.InteractionSystem.Hand rightHand;


    // Start is called before the first frame update
    void Start()
    {
        respawnPoint = GameObject.Find("RightHand");//make objects respawn as child of the right hand
        rightHand = respawnPoint.GetComponent<Valve.VR.InteractionSystem.Hand>();//will break if respawn point is not hand
    }

    //accessed by hardware button
    public void Press()
    {
        Debug.Log("Individual button press");
        //if there's nothing in the pocket, get what you're holding and put it in the pocket
        if (pocketObj == null)
        {
            heldObj = rightHand.currentAttachedObject;//this may cause a weird issue where you're eternally holding the object
            Debug.Log("currentAttachedObject to put into pocket: " + heldObj);//runs
            rightHand.DetachObject(heldObj, false);
            heldObj.SetActive(false);
            pocketObj = heldObj;//check what pocket object and held object are
            Debug.Log("heldObj is " + heldObj + ", pocketObj is " + pocketObj);//this is also correct
            //heldObj = null;//unclear if necessary
        }
        //if there's something in the pocket, take it out
        else
        {
            Debug.Log("What is the current attached object /before/ you take the " + pocketObj + " out of the pocket? " + rightHand.currentAttachedObject);
            Debug.Log("Take this item out of pocket: " + pocketObj);
            pocketObj.transform.position = respawnPoint.transform.position;
            pocketObj.SetActive(true);
            pocketObj = null;
        }
    }
}
