﻿/// <summary>
/// Adapted from CodeArtist.mx (original attribution below)
/// adapted for VR (using SteamVR) by asimonso@mit.edu
/// --march 2020
/// 
/// 
/// CodeArtist.mx 2015
/// This is the main class of the project, its in charge of raycasting to a model and place brush prefabs infront of the canvas camera.
/// If you are interested in saving the painted texture you can use the method at the end and should save it to a file.
/// </summary>
///FROM TEXTURE PAINTER FRESH COPY

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Valve.VR;

public enum Painter_BrushMode{PAINT,DECAL};
public class TexturePainter : MonoBehaviour {
	public GameObject brushCursor,brushContainer; //The cursor that overlaps the model and our container for the brushes painted
	public Camera sceneCamera,canvasCam;  //The camera that looks at the model, and the camera that looks at the canvas.
	public Sprite cursorPaint,cursorDecal; // Cursor for the differen functions 
	public RenderTexture canvasTexture; // Render Texture that looks at our Base Texture and the painted brushes
	public Material baseMaterial; // The material of our base texture (Were we will save the painted texture)
    public string DEBUGGINGFLAGNAME;//DELETE THIS IT WAS FOR DEBUGGING
    public MeshCollider thisMesh;

	Painter_BrushMode mode; //Our painter mode (Paint brushes or decals)
	public float brushSize=1.0f; //The size of our brush--we can change this!
	Color brushColor; //The selected color
	int brushCounter=0,MAX_BRUSH_COUNT=1000; //To avoid having millions of brushes
	bool saving=false; //Flag to check if we are saving the texture

    //VR additions
    public SteamVR_ActionSet actionSetEnable;
    public SteamVR_Action_Boolean paint;
    string inputDevice = "";
    public GameObject paintbrush; //whatever you want to use to paint
    public PaintSelector paintselector;

    void Update () {
		//brushColor = ColorSelector.GetColor ();	//Updates our painted color with the selected color-----you turned this off, but may need to not
		if (Input.GetMouseButton(0)) {
            Debug.Log("Input device is mouse???");
            inputDevice = "mouse";
			DoAction();
		}
        if (paint.GetState(SteamVR_Input_Sources.Any))
        {
            Debug.Log("The paint action has been requested");
            inputDevice = "VR";
            DoAction();
        }
            UpdateBrushCursor ();
	}

    //for Paint Selector to tell Texture Painter about whatever brush we're holding
    public void UpdateBrush(GameObject newBrush)
    {
        paintbrush = newBrush;
        //the dividing by 2 is an arbitrary number you can budge
        brushSize = paintbrush.transform.localScale.x/2.0f;//the axis should all be the same, we just needed to pick one
        brushCursor.transform.localScale = Vector3.one * brushSize / 2.0f;
    }

    //The main action, instantiates a brush or decal entity at the clicked position on the UV map
    void DoAction(){	
		if (saving)
			return;
		Vector3 uvWorldPosition=Vector3.zero;		
		if(HitTestUVPosition(ref uvWorldPosition)){
			GameObject brushObj;
			if(mode==Painter_BrushMode.PAINT){

				brushObj=(GameObject)Instantiate(Resources.Load("TexturePainter-Instances/BrushEntity")); //Paint a brush
                //brushObj.GetComponent<SpriteRenderer>().color=brushColor; //Set the brush color--original
                brushObj.GetComponent<SpriteRenderer>().color = paintselector.selectedPaint;//--for VR
            }
            else
            {
				brushObj=(GameObject)Instantiate(Resources.Load("TexturePainter-Instances/DecalEntity")); //Paint a decal
			}
			//brushColor.a=brushSize*2.0f; // Brushes have alpha to have a merging effect when painted over.----------------------you turned this off, but may need to turn it back on again
			brushObj.transform.parent=brushContainer.transform; //Add the brush to our container to be wiped later
			brushObj.transform.localPosition=uvWorldPosition; //The position of the brush (in the UVMap)
			brushObj.transform.localScale=Vector3.one*brushSize;//The size of the brush--can we just...change this?
		}
		brushCounter++; //Add to the max brushes
		if (brushCounter >= MAX_BRUSH_COUNT) { //If we reach the max brushes available, flatten the texture and clear the brushes
			brushCursor.SetActive (false);
			saving=true;
			Invoke("SaveTexture",0.1f);
			
		}
	}
	//To update at realtime the painting cursor on the mesh
	void UpdateBrushCursor(){
		Vector3 uvWorldPosition=Vector3.zero;
        if (HitTestUVPosition (ref uvWorldPosition) && !saving) {
            brushCursor.SetActive(true);
			brushCursor.transform.position =uvWorldPosition+brushContainer.transform.position;
        }
        else {
			brushCursor.SetActive(false);
		}
	}
    //Returns the position on the texuremap according to a hit in the mesh collider
    bool HitTestUVPosition(ref Vector3 uvWorldPosition)
    {
        RaycastHit hit;
        if (inputDevice == "mouse")
        {
            Vector3 cursorPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);
            Ray cursorRay = sceneCamera.ScreenPointToRay(cursorPos);
            if (Physics.Raycast(cursorRay, out hit, 200))
            {
                MeshCollider meshCollider = hit.collider as MeshCollider;
                if (meshCollider == null || meshCollider.sharedMesh == null)
                    return false;
                Vector2 pixelUV = new Vector2(hit.textureCoord.x, hit.textureCoord.y);
                uvWorldPosition.x = pixelUV.x - canvasCam.orthographicSize;//To center the UV on X
                uvWorldPosition.y = pixelUV.y - canvasCam.orthographicSize;//To center the UV on Y
                uvWorldPosition.z = 0.0f;
                return true;
            }
            return false;
        }
        if (inputDevice == "VR")
        {
            Debug.DrawRay(paintbrush.transform.position, paintbrush.transform.forward);
            if (Physics.Raycast(paintbrush.transform.position, paintbrush.transform.forward, out hit, 200))
            {
                MeshCollider meshCollider = hit.collider as MeshCollider;
                if (meshCollider == null || meshCollider.sharedMesh == null || meshCollider != thisMesh)
                    return false;
                Vector2 pixelUV = new Vector2(hit.textureCoord.x, hit.textureCoord.y);
                uvWorldPosition.x = pixelUV.x - canvasCam.orthographicSize;//To center the UV on X
                uvWorldPosition.y = pixelUV.y - canvasCam.orthographicSize;//To center the UV on Y
                uvWorldPosition.z = 0.0f;
                Debug.Log("hit something that we can paint: " + thisMesh.gameObject.name);
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }

    }
    //Sets the base material with a our canvas texture, then removes all our brushes
    void SaveTexture(){		
		brushCounter=0;
		System.DateTime date = System.DateTime.Now;
		RenderTexture.active = canvasTexture;
		Texture2D tex = new Texture2D(canvasTexture.width, canvasTexture.height, TextureFormat.RGB24, false);		
		tex.ReadPixels (new Rect (0, 0, canvasTexture.width, canvasTexture.height), 0, 0);
		tex.Apply ();
		RenderTexture.active = null;
		baseMaterial.mainTexture =tex;	//Put the painted texture as the base
		foreach (Transform child in brushContainer.transform) {//Clear brushes
			Destroy(child.gameObject);
		}
		//StartCoroutine ("SaveTextureToFile"); //Do you want to save the texture? This is your method!
		Invoke ("ShowCursor", 0.1f);
	}
	//Show again the user cursor (To avoid saving it to the texture)
	void ShowCursor(){	
		saving = false;
	}

	////////////////// PUBLIC METHODS //////////////////

	public void SetBrushMode(Painter_BrushMode brushMode){ //Sets if we are painting or placing decals
		mode = brushMode;
		brushCursor.GetComponent<SpriteRenderer> ().sprite = brushMode == Painter_BrushMode.PAINT ? cursorPaint : cursorDecal;
	}
	public void SetBrushSize(float newBrushSize){ //Sets the size of the cursor brush or decal
		brushSize = newBrushSize;
		brushCursor.transform.localScale = Vector3.one * brushSize/5.0f;//the dividing by 2 is an arbitrary number you can budge
	}

	////////////////// OPTIONAL METHODS //////////////////

	#if !UNITY_WEBPLAYER 
		IEnumerator SaveTextureToFile(Texture2D savedTexture){		
			brushCounter=0;
			string fullPath=System.IO.Directory.GetCurrentDirectory()+"\\UserCanvas\\";
			System.DateTime date = System.DateTime.Now;
			string fileName = "CanvasTexture.png";
			if (!System.IO.Directory.Exists(fullPath))		
				System.IO.Directory.CreateDirectory(fullPath);
			var bytes = savedTexture.EncodeToPNG();
			System.IO.File.WriteAllBytes(fullPath+fileName, bytes);
			Debug.Log ("<color=orange>Saved Successfully!</color>"+fullPath+fileName);
			yield return null;
		}
	#endif
}
