﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

// This script gets the color of a sprite in realtime 
//There's a lot of duplicate code between this and Texture Painter which you could probably refactor, public variables which have to be hooked up in this which it could just get from there

public class SpriteColor : MonoBehaviour
{
    public static Color GetColor { get; set; }

    public SpriteRenderer palette;//this isn't the palette gameobject, it's the color picker sprite
    public GameObject thumb;//what should this be?
    Color finalColor;
    bool dragging = false;//there are no if conditions based on this in this script, but there might be elsewhere

    //VR additions
    public SteamVR_ActionSet actionSetEnable;
    public SteamVR_Action_Boolean paint;
    string inputDevice = "";
    public GameObject paintbrush; //whatever you want to use to paint
    public Camera VRCamera;//this needs to be manually linked in the inspector

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Debug.Log("Input device is mouse???");
            inputDevice = "mouse";
            dragging = true;

            //replace ray with controller ray
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitData;

            if (Physics.Raycast(ray, out hitData, 1000) && hitData.transform.tag == "Palette")
            {
                thumb.transform.position = hitData.point;
            }

            GetSpritePixelColorUnderMousePointer(palette, out finalColor);
        }
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
        }
        if (paint.GetState(SteamVR_Input_Sources.Any))//vr version of getmousebutton
        {
            Debug.Log("Painting in VR, according to SpriteColor");//we get here
            inputDevice = "VR";
            GetSpritePixelColorUnderMousePointer(palette, out finalColor);
        }
        //if dragging turns out to be important, do a vr equivalent of mouse button up here
    }
    //for Paint Selector to tell Sprite Color about whatever brush we're holding
    public void UpdateBrush(GameObject newBrush)
    {
        paintbrush = newBrush;
    }
    public bool GetSpritePixelColorUnderMousePointer(SpriteRenderer spriteRenderer, out Color color)
    {
        color = new Color();
        if(inputDevice == "mouse")
        {
            Camera cam = Camera.main;

            Vector2 mousePos = Input.mousePosition;
            Vector2 viewportPos = cam.ScreenToViewportPoint(mousePos);
            if (viewportPos.x < 0.0f || viewportPos.x > 1.0f || viewportPos.y < 0.0f || viewportPos.y > 1.0f) return false; // out of viewport bounds-- will it still work if you just remove this error handling?

            // Cast a ray from viewport point into world
            Ray ray = cam.ViewportPointToRay(viewportPos);//can you do this with VR camera?
            // Check for intersection with sprite and get the color
            return IntersectsSprite(spriteRenderer, ray, out color);
        }
        else if(inputDevice == "VR")
        {
            //repeat above with VR camera and ray
            Camera cam = VRCamera;
            //Cast ray from paintbrush
            Ray ray = new Ray(paintbrush.transform.position, paintbrush.transform.forward);
            // Check for intersection with sprite and get the color
            return IntersectsSprite(spriteRenderer, ray, out color);
        }
        return false;//should only run if something is wrong
    }

    private bool IntersectsSprite(SpriteRenderer spriteRenderer, Ray ray, out Color color)
    {
        //most of what's happening here comes from the fact that we need to convert the sprite to a texture in order to use texture.GetPixel
        //is it possible / easier to just use a texture in the first place?
        color = new Color();
        if (spriteRenderer == null) return false;

        Sprite sprite = spriteRenderer.sprite;
        if (sprite == null) return false;

        Texture2D texture = sprite.texture;
        if (texture == null) return false;


        // Craete a plane so it has the same orientation as the sprite transform
        Plane plane = new Plane(transform.forward, transform.position);

        // Intersect the ray and the plane
        float rayIntersectDist; // the distance from the ray origin to the intersection point
        if (!plane.Raycast(ray, out rayIntersectDist)) return false; 

        // Convert world position to sprite position
        // worldToLocalMatrix.MultiplyPoint3x4 returns a value from based on the texture dimensions (+/- half texDimension / pixelsPerUnit) )
        // 0, 0 corresponds to the center of the TEXTURE ITSELF, not the center of the trimmed sprite textureRect

        Vector3 spritePos = spriteRenderer.worldToLocalMatrix.MultiplyPoint3x4(ray.origin + (ray.direction * rayIntersectDist));
        Rect textureRect = sprite.textureRect;
        float pixelsPerUnit = sprite.pixelsPerUnit;
        float halfRealTexWidth = texture.width * 0.5f; // use the real texture width here because center is based on this -- probably won't work right for atlases
        float halfRealTexHeight = texture.height * 0.5f;
        // Convert to pixel position, offsetting so 0,0 is in lower left instead of center
        int texPosX = (int)(spritePos.x * pixelsPerUnit + halfRealTexWidth);
        int texPosY = (int)(spritePos.y * pixelsPerUnit + halfRealTexHeight);
        // Check if pixel is within texture
        if (texPosX < 0 || texPosX < textureRect.x || texPosX >= Mathf.FloorToInt(textureRect.xMax)) return false; // out of bounds
        if (texPosY < 0 || texPosY < textureRect.y || texPosY >= Mathf.FloorToInt(textureRect.yMax)) return false; // out of bounds
                                                                                                                   // Get pixel color
        color = texture.GetPixel(texPosX, texPosY);
        GetColor = color;

        return true;
    }

}
