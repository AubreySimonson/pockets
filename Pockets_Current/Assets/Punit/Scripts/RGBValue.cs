﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// This script is attached to the RGBValues gameobject
// Shows description of RGB values using sliders

public class RGBValue : MonoBehaviour
{
    public Slider red;
    public Slider green;
    public Slider blue;
    public GameObject[] colorFaces;

    [SerializeField]
    float speed = 300f;
    bool dragging = false;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            dragging = true;
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            dragging = false;
        }
    }

    void FixedUpdate()
    {
        if (dragging)
        {
            float x = Input.GetAxis("Mouse X") * speed * Time.fixedDeltaTime;
            float y = Input.GetAxis("Mouse Y") * speed * Time.fixedDeltaTime;

            rb.AddTorque(Vector3.down * x);
            rb.AddTorque(Vector3.right * y);
        }
    }

    public void OnChange()
    {
        foreach (GameObject g in colorFaces)
        {
            g.GetComponent<Renderer>().material.color = new Color(red.value, green.value, blue.value);
        }
    }
}
