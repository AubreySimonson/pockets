﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndividualBrush : MonoBehaviour
{
    public float finalSize;
    
    public float Size
    {
        get
        {
            return finalSize;
        }

        set
        {
            finalSize = value;
        }
    }

    void Update()
    {

    }
    
}
