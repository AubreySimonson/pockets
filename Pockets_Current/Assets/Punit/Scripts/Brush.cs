﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brush : MonoBehaviour
{
    GameObject selectedBrush;
    GameObject currBrush, newBrush;
    public List<GameObject> brushHolder = new List<GameObject>();
    bool dragging;
    bool flagBrush, moveBrush;
    public GameObject originPos;
    public GameObject paintBrush;
    public TexturePainter painter;

    public GameObject brushSize;
    public Slider brushSizeSlider;
    float scaleFactor;

    void Update()
    {
        SpawnNewBrush();

        if (dragging)
        {
            var pos = Input.mousePosition;
            pos.z = -Camera.main.transform.position.z;
            currBrush.transform.position = Camera.main.ScreenToWorldPoint(pos);

            if (Input.GetMouseButtonUp(0))
            {
                dragging = false;
                selectedBrush = currBrush;
                brushHolder.Add(currBrush);
                DisableAllExceptSelectedBrush();
                selectedBrush.GetComponent<IndividualBrush>().Size = brushSizeSlider.value;
                currBrush.tag = "Brush";
                flagBrush = true;

                if (currBrush.transform.position != originPos.transform.position)
                {
                    newBrush = Instantiate(paintBrush, transform);
                    newBrush.GetComponent<IndividualBrush>().enabled = false;
                }
            }
        }

        if (moveBrush)
        {
            var pos = Input.mousePosition;
            pos.z = -Camera.main.transform.position.z;
            currBrush.transform.position = Camera.main.ScreenToWorldPoint(pos);

            if (Input.GetMouseButtonUp(0))
            {
                moveBrush = false;
                selectedBrush = currBrush;
                DisableAllExceptSelectedBrush();
            }
        }

        if (brushHolder.Count > 0 && flagBrush)
        {
            painter.SetBrushSize(selectedBrush.GetComponent<IndividualBrush>().Size);
            flagBrush = false;
        }

    }

    public void ChangeScale()
    {
        scaleFactor = brushSizeSlider.value * 15;
        brushSize.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
    }

    void SpawnNewBrush()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("InitialBrush"))
            {
                dragging = true;
                currBrush = hit.collider.gameObject;

                if (!currBrush.GetComponent<IndividualBrush>().enabled)
                {
                    currBrush.GetComponent<IndividualBrush>().enabled = true;
                }
            }

            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("Brush"))
            {
                moveBrush = true;
                currBrush = hit.collider.gameObject;
            }
        }
    }

    void DisableAllExceptSelectedBrush()
    {
        for (int i = 0; i < brushHolder.Count; i++)
        {
            if (brushHolder[i] == selectedBrush)
            {
                brushHolder[i].GetComponent<IndividualBrush>().enabled = true;
                continue;
            }

            brushHolder[i].GetComponent<IndividualBrush>().enabled = false;
        }
    }
}
