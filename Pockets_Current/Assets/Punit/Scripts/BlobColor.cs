﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to the blob prefab
// Assigns new color to the blob prefab

public class BlobColor : MonoBehaviour
{
    GameObject selectedUnit = null; // holds the selected the gameobject

    void Update()
    {
        // spontaneously displays the changing color of the blob until the final color is assigned
        if (selectedUnit != null)
        {
            selectedUnit.GetComponent<Renderer>().material.color = SpriteColor.GetColor;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // if the hit object is blob, it will be assigned to the selectedUnit until it's color is assigned
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Blob" && selectedUnit == null)
            {
                selectedUnit = hit.collider.gameObject;
            }

            // if the hit object is palette, color will be assigned at which the palette is hit
            else if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Palette" && selectedUnit != null)
            {
                selectedUnit.GetComponent<Renderer>().material.color = SpriteColor.GetColor;
                selectedUnit = null;
            }
            
        }

    }

}
