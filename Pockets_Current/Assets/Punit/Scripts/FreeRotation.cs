﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRotation : MonoBehaviour
{
    public Transform player;

    // Update is called once per frame
    void Update()
    {

        //direction = destination - source

        Vector3 directiontoFace = transform.position - player.position;
        Debug.DrawRay(player.position, (transform.position - player.position), Color.red);

        Quaternion targetRotation = Quaternion.LookRotation(directiontoFace);

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2);
    }
}
