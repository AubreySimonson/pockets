﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to the Color Picker gameobject
// It checks and destroys the instantiated blob if it's still inside/colliding with the palette

public class DestroyOnCollision : MonoBehaviour
{
    public static bool isColliding = false;

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Blob")
        {
            //Debug.Log("Collision!");
            isColliding = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        isColliding = false;
    }
}
