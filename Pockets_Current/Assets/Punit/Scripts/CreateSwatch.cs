﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to the palette gameobject
// Generates a spherical color blob from the color picker

public class CreateSwatch : MonoBehaviour
{
    public GameObject blob; // spherical blob prefab
    GameObject spawn; // holds the spawned gameobjects transiently until the mouse button isn't released
    bool flagColor; // checks whether the color is assigned to the spawned gameobject
    bool dragging = false;

    void Update()
    {
        Spawn();
        
        // updates the spawned gamobject's position until the mouse button is held
        if (spawn != null)
        {
            var pos = Input.mousePosition;
            pos.z = -Camera.main.transform.position.z;
            spawn.transform.position = Camera.main.ScreenToWorldPoint(pos);
        }

        // when the mouse button is released, checks some edge cases and flags
        if (Input.GetMouseButtonUp(2))
        {
            dragging = false;

            if (DestroyOnCollision.isColliding == true)
            {
                Destroy(spawn);
            }

            spawn = null;
            flagColor = false;
        }

    }

    void Spawn()
    {
        RaycastHit rayCastHit;
        Ray rayCast = Camera.main.ScreenPointToRay(Input.mousePosition);

        // if the hit object is palette and the mouse button is pressed; a blob will be instantiated
        if (Physics.Raycast(rayCast, out rayCastHit) && rayCastHit.collider.CompareTag("Palette"))
        {
            //var e = Event.current;

            if (Input.GetMouseButton(2) && !flagColor)
            {
                dragging = true;
                var pos = Input.mousePosition;
                pos.z = -Camera.main.transform.position.z;
                pos = Camera.main.ScreenToWorldPoint(pos);
                spawn = Instantiate(blob, pos, Quaternion.identity);
                spawn.GetComponent<Renderer>().material.color = SpriteColor.GetColor;
                flagColor = true;
            }
        }

    }

}