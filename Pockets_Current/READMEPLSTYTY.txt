Hi.  This project contains a safe copy of texture painter which can paint with both the mouse and VR.  You really should put this up in the asset store, but will probably never get to it.  The mouse still works to paint. 

If you're rooting around in these files looking for a fresh copy of texture painter-- you've had luck with searching "colorselector" in the project window of other projects if you're having a hard time finding it in the asset store.  Need a version of VR Texture painter?  Don't change things about this one without making a copy!  What if you put it in the Asset store?  Or at least github????

--asimonso@mit.edu march 2020